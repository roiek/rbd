--
-- Licensed to JumpMind Inc under one or more contributor
-- license agreements.  See the NOTICE file distributed
-- with this work for additional information regarding
-- copyright ownership.  JumpMind Inc licenses this file
-- to you under the GNU General Public License, version 3.0 (GPLv3)
-- (the "License"); you may not use this file except in compliance
-- with the License.
--
-- You should have received a copy of the GNU General Public License,
-- version 3.0 (GPLv3) along with this library; if not, see
-- <http://www.gnu.org/licenses/>.
--
-- Unless required by applicable law or agreed to in writing,
-- software distributed under the License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
-- KIND, either express or implied.  See the License for the
-- specific language governing permissions and limitations
-- under the License.
--

------------------------------------------------------------------------------
-- Sample Symmetric Configuration
------------------------------------------------------------------------------

insert into sym_channel 
(channel_id, processing_order, max_batch_size, enabled, description)
values('rbd', 1, 100000, 1, 'Kanal glowny');

--insert into sym_node_group (node_group_id) values ('central');
insert into sym_node_group (node_group_id) values ('external');

insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action) values ('central', 'external', 'W');
insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action) values ('external', 'central', 'P');

insert into sym_trigger 
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('tickets','ticket','rbd',current_timestamp,current_timestamp);

insert into sym_router 
(router_id,source_node_group_id,target_node_group_id,router_type,create_time,last_update_time)
values('central_2_external', 'central', 'external', 'default',current_timestamp, current_timestamp);

insert into sym_router
(router_id,source_node_group_id,target_node_group_id,router_type,create_time,last_update_time)
values('external_2_central', 'external', 'central', 'default',current_timestamp, current_timestamp);

insert into sym_trigger_router 
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('tickets','central_2_external', 100, current_timestamp, current_timestamp);

insert into sym_trigger_router 
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('tickets','external_2_central', 200, current_timestamp, current_timestamp);
