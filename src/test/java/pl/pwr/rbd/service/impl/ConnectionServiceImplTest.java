package pl.pwr.rbd.service.impl;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.concurrent.ThreadLocalRandom.current;
import static org.mockito.BDDMockito.given;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.pwr.rbd.model.City;
import pl.pwr.rbd.model.Connection;
import pl.pwr.rbd.repository.ConnectionRepository;


@RunWith(MockitoJUnitRunner.class)
public class ConnectionServiceImplTest {

	@InjectMocks private ConnectionServiceImpl connectionService;

	@Mock private ConnectionRepository connectionRepository;

	@Test
	public void shouldTest() throws Exception {

		List<String> allWord = Files.readAllLines(Paths.get("C:\\Users\\T\\Downloads\\słowa\\slowa.txt"));

		firstCombo("wsłoio").stream()
			.map(this::permutation)
			.flatMap(Set::stream)
			.distinct()
			.filter(allWord::contains)
			.collect(Collectors.groupingBy(String::length))
			.forEach((integer, strings) -> {
				System.out.println("--------" + integer + "--------");
				strings.forEach(System.out::println);
			});

		// given

		// when

		// then

	}
	public Set<String> permutation(String s) {
		// The result
		Set<String> res = new HashSet<>();
		// If input string's length is 1, return {s}
		if (s.length() == 1) {
			res.add(s);
		} else if (s.length() > 1) {
			int lastIndex = s.length() - 1;
			// Find out the last character
			String last = s.substring(lastIndex);
			// Rest of the string
			String rest = s.substring(0, lastIndex);
			// Perform permutation on the rest string and
			// merge with the last character
			res = merge(permutation(rest), last);
		}
		return res;
	}

	Set<String> firstCombo(String s) {

		Set<String> result = new HashSet<>();

		combo("", s, result);

		return result;

	}

	Set<String> combo(String prefix, String s, Set<String> set)
	{
		int N = s.length();

		set.add(prefix);

		for (int i = 0 ; i < N ; i++)
			combo(prefix + s.charAt(i), s.substring(i+1), set);

		return set;
	}

	/**
	 * @param list a result of permutation, e.g. {"ab", "ba"}
	 * @param c    the last character
	 * @return     a merged new list, e.g. {"cab", "acb" ... }
	 */
	public Set<String> merge(Set<String> list, String c) {
		Set<String> res = new HashSet<String>();
		// Loop through all the string in the list
		for (String s : list) {
			// For each string, insert the last character to all possible postions
			// and add them to the new list
			for (int i = 0; i <= s.length(); ++i) {
				String ps = new StringBuffer(s).insert(i, c).toString();
				res.add(ps);
			}
		}
		return res;
	}

	@Test
	public void should() throws Exception {

		// given
		String wroclawName = "Wrocław";
		String poznanName = "Poznań";
		String gdanskName = "Gdańsk";

		City wroclaw = new City(wroclawName);
		City poznan = new City(poznanName);
		City gdansk = new City(gdanskName);

		List<City> route1 = newArrayList(wroclaw, poznan, gdansk);
		List<City> route2 = newArrayList(gdansk, wroclaw, poznan );
		List<City> route3 = newArrayList(wroclaw, gdansk);

		Connection connection1 = new Connection(randomLong(), route1);
		Connection connection2 = new Connection(randomLong(), route2);
		Connection connection3 = new Connection(randomLong(), route3);

		given(connectionRepository.findAll()).willReturn(newArrayList(connection1, connection2, connection3));

		// dateOfDeparture
		List<Connection> result = connectionService.findAllByStartAndEnd(wroclawName, poznanName);

		// then
		Assertions.assertThat(result).containsExactlyInAnyOrder(connection1, connection2);

	}

	private long randomLong() {

		return current().nextLong();
	}

}