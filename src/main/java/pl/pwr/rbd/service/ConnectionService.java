package pl.pwr.rbd.service;

import java.util.List;

import pl.pwr.rbd.model.Connection;


public interface ConnectionService {
    List<Connection> findAll();

    List<Connection> findAllByStartAndEnd(String start, String end);
}
