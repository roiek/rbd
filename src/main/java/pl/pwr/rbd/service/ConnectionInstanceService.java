package pl.pwr.rbd.service;

import java.util.Optional;

import pl.pwr.rbd.model.ConnectionInstance;
import pl.pwr.rbd.model.Ticket;
import pl.pwr.rbd.mvc.ConnectionInstanceDto;


public interface ConnectionInstanceService {

	Optional<Ticket> buyTicket(ConnectionInstance connectionInstance);
	int numberOfTicketsBought(ConnectionInstance connectionInstance);

	ConnectionInstanceDto wrapToDto(ConnectionInstance connectionInstance);

	void removeTicket(ConnectionInstance connectionInstance);
}
