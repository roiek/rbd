package pl.pwr.rbd.service.impl;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.pwr.rbd.model.City;
import pl.pwr.rbd.model.Connection;
import pl.pwr.rbd.repository.CityRepository;
import pl.pwr.rbd.repository.ConnectionRepository;
import pl.pwr.rbd.service.ConnectionService;


@Service
public class ConnectionServiceImpl implements ConnectionService {

	private final ConnectionRepository connectionRepository;
	private final CityRepository cityRepository;

	@Autowired
	public ConnectionServiceImpl(ConnectionRepository connectionRepository,
		CityRepository cityRepository) {

		this.connectionRepository = connectionRepository;
		this.cityRepository = cityRepository;
	}

	@Override
	public List<Connection> findAll() {

		return connectionRepository.findAll();
	}

	@Override
	public List<Connection> findAllByStartAndEnd(String startCityId, String endCityId) {

		final City start = cityRepository.findOne(startCityId);
		final City end = cityRepository.findOne(endCityId);

		return findAll().stream()
			.filter(containsCity(start))
			.filter(containsCity(end))
			.filter(isInRightOrder(start, end))
			.collect(Collectors.toList());
	}

	private Predicate<? super Connection> containsCity(City city) {

		return connection -> connection.getAllCity().contains(city);
	}

	private Predicate<? super Connection> isInRightOrder(City start, City end) {

		return connection -> connection.getAllCity().indexOf(start) < connection.getAllCity().indexOf(end);
	}

}
