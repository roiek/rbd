package pl.pwr.rbd.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.pwr.rbd.model.ConnectionInstance;
import pl.pwr.rbd.model.Ticket;
import pl.pwr.rbd.mvc.ConnectionInstanceDto;
import pl.pwr.rbd.repository.TicketRepository;
import pl.pwr.rbd.service.ConnectionInstanceService;

@Service
public class ConnectionInstanceServiceImpl implements ConnectionInstanceService{

	@Autowired private TicketRepository ticketRepository;

	@Override
	public int numberOfTicketsBought(ConnectionInstance connectionInstance) {

		return ticketRepository.countAllByConnectionInstance(connectionInstance);
	}

	@Override
	public Optional<Ticket> buyTicket(ConnectionInstance connectionInstance) { // kupno jednego biletu

		final int capacity = connectionInstance.getCapacity();
		final int alreadyBoughtTickets = ticketRepository.countAllByConnectionInstance(connectionInstance);

		if (capacity <= alreadyBoughtTickets) {
			return Optional.empty();
		}
		else {

			return Optional.ofNullable(ticketRepository.save(new Ticket(connectionInstance)));
		}
	}

	@Override
	public ConnectionInstanceDto wrapToDto(ConnectionInstance connectionInstance) {

		return new ConnectionInstanceDto(
			connectionInstance.getId(),
			connectionInstance.getDateOfDeparture(),
			connectionInstance.getCapacity(),
			numberOfTicketsBought(connectionInstance),
			connectionInstance.getTrainName());
	}

	@Override
	public void removeTicket(ConnectionInstance connectionInstance) {

		ticketRepository.delete(ticketRepository.findTop1ByConnectionInstance(connectionInstance));
	}
}
