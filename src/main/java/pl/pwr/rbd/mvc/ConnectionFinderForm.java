package pl.pwr.rbd.mvc;

import java.time.LocalDate;


public class ConnectionFinderForm {

	private String startCityId;
	private String endCityId;
	private LocalDate date;

	public String getStartCityId() {

		return startCityId;
	}

	public void setStartCityId(String startCityId) {

		this.startCityId = startCityId;
	}

	public String getEndCityId() {

		return endCityId;
	}

	public void setEndCityId(String endCityId) {

		this.endCityId = endCityId;
	}

	public LocalDate getDate() {

		return date;
	}

	public void setDate(LocalDate date) {

		this.date = date;
	}
}
