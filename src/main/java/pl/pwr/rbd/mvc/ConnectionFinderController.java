package pl.pwr.rbd.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import pl.pwr.rbd.model.City;
import pl.pwr.rbd.repository.CityRepository;
import pl.pwr.rbd.service.ConnectionService;


@Controller
public class ConnectionFinderController {

	@Autowired private CityRepository cityRepository;
	@Autowired private ConnectionService connectionService;

	@ModelAttribute("cities")
	public List<City> cities() {
		return cityRepository.findAll();
	}

	@ModelAttribute("connectionFinderForm")
	public ConnectionFinderForm connectionFinderForm() {
		return new ConnectionFinderForm();
	}

	@GetMapping("/")
	public String getForm() {
		return "connectionFinder";
	}

	@GetMapping("/search")
	public String getAvailableConnections(@ModelAttribute("connectionFinderForm") ConnectionFinderForm connectionFinderForm,
		Model model) {

		model.addAttribute("foundConnections", connectionService.findAllByStartAndEnd(connectionFinderForm.getStartCityId(), connectionFinderForm.getEndCityId()));
		model.addAttribute("connectionFinderForm", connectionFinderForm);

		return "connectionFinder";
	}
}
