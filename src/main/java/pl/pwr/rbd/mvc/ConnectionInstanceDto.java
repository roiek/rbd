package pl.pwr.rbd.mvc;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class ConnectionInstanceDto {

	private long id;
	private LocalDate dateOfDeparture;
	private int capacity;
	private int numberOfTicketsBought;
	private String trainName;
}
