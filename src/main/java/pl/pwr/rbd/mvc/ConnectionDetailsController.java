package pl.pwr.rbd.mvc;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.pwr.rbd.model.Connection;
import pl.pwr.rbd.model.ConnectionInstance;
import pl.pwr.rbd.repository.ConnectionInstanceRepository;
import pl.pwr.rbd.repository.ConnectionRepository;
import pl.pwr.rbd.service.ConnectionInstanceService;


@Controller
public class ConnectionDetailsController {

	@Autowired private ConnectionInstanceRepository connectionInstanceRepository;
	@Autowired private ConnectionRepository connectionRepository;
	@Autowired private ConnectionInstanceService connectionInstanceService;

	@GetMapping("/details/{id}")
	public String connectionDetails(@PathVariable long id, Model model) {

		Connection connection = connectionRepository.findOne(id);
		List<ConnectionInstanceDto> connectionInstances = connectionInstanceRepository.findAllByConnection(connection).stream()
			.map(connectionInstanceService::wrapToDto)
			.collect(toList());

		model.addAttribute("connection", connection);
		model.addAttribute("connectionInstances", connectionInstances);

		return "details";
	}

	@PostMapping("/details/{id}/buyTicket")
	public String buyTicket(@RequestParam long instanceId, RedirectAttributes redirectAttributes, @PathVariable long id) {

		ConnectionInstance connectionInstance = connectionInstanceRepository.findOne(instanceId);
		connectionInstanceService.buyTicket(connectionInstance)
			.ifPresent(ticket -> redirectAttributes.addFlashAttribute("message", "Bilet zakupiony"));

		return "redirect:/details/" + id;
	}

	@PostMapping("/details/{id}/removeTicket")
	public String removeTicket(@RequestParam long instanceId, RedirectAttributes redirectAttributes, @PathVariable long id) {

		ConnectionInstance connectionInstance = connectionInstanceRepository.findOne(instanceId);
		connectionInstanceService.removeTicket(connectionInstance);

		return "redirect:/details/" + id;
	}
}
