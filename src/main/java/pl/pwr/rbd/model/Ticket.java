package pl.pwr.rbd.model;

import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "ticket")
@NoArgsConstructor
@Data
public class Ticket {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @ManyToOne()
    @JoinColumn(name="connectionInstance")
    private ConnectionInstance connectionInstance;

	public Ticket(ConnectionInstance connectionInstance) {

		this.connectionInstance = connectionInstance;
	}
}
