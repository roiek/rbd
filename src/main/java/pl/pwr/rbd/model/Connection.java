package pl.pwr.rbd.model;

import static java.util.stream.Collectors.joining;
import static javax.persistence.GenerationType.AUTO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Connection {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @ManyToMany
    @JoinTable(name = "connection_cities")
	@OrderColumn(name = "list_index")
    private List<City> allCity = new ArrayList<>();

	public String getLabel() {

		return allCity.stream()
			.map(City::getName)
			.collect(joining(" - "));
	}
}
