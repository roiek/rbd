package pl.pwr.rbd.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConnectionInstance {

	@Id
	private long id;

	private LocalDate dateOfDeparture;

	private String trainName;

	@ManyToOne
	private Connection connection;

	private int capacity;

}
