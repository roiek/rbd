package pl.pwr.rbd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pwr.rbd.model.Connection;

import java.util.List;


@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long> {

   // List<Connection> findByC

}
