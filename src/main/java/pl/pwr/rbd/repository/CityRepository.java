package pl.pwr.rbd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pwr.rbd.model.City;

@Repository
public interface CityRepository extends JpaRepository<City, String> {

}
