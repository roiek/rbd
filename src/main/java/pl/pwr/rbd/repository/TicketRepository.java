package pl.pwr.rbd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.pwr.rbd.model.ConnectionInstance;
import pl.pwr.rbd.model.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

	int countAllByConnectionInstance(ConnectionInstance connectionInstance);
	List<Ticket> findTop1ByConnectionInstance(ConnectionInstance connectionInstance);
}
