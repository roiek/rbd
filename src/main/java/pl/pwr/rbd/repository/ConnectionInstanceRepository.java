package pl.pwr.rbd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.pwr.rbd.model.Connection;
import pl.pwr.rbd.model.ConnectionInstance;
import pl.pwr.rbd.mvc.ConnectionInstanceDto;


@Repository
public interface ConnectionInstanceRepository  extends JpaRepository<ConnectionInstance, Long> {

	List<ConnectionInstance> findAllByConnection(Connection connection);
}
