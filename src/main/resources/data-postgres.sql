
insert INTO cities values  ('Wroclaw');
insert INTO cities values  ('Poznan');
insert INTO cities values  ('Gdansk');
insert INTO cities values  ('Warszawa');

insert into connection values (1);
insert into connection values (2);
insert into connection values (3);

insert into connection_cities values (1, 'Wroclaw', 0);
insert into connection_cities values (1, 'Poznan', 1);
insert into connection_cities values (1, 'Gdansk', 2);
insert into connection_cities values (2, 'Wroclaw', 0);
insert into connection_cities values (2, 'Gdansk', 1);
insert into connection_cities values (2, 'Poznan', 2);
insert into connection_cities values (3, 'Warszawa', 0);
insert into connection_cities values (3, 'Gdansk', 1);
insert into connection_cities values (3, 'Poznan', 2);


insert into connection_instance values (4, 10, now(), 'Pociag1', 1);
insert into connection_instance values (5, 11, now() + interval '1 day', 'Pociag2', 1);
insert into connection_instance values (6, 12, now() + interval '2 days', 'Pociag3', 1);
insert into connection_instance values (7, 8, now(), 'Pociag4', 2);
insert into connection_instance values (8, 9, now() + interval '5 days', 'Pociag5', 2);
insert into connection_instance values (9, 10, now() + interval '6 days', 'Pociag6', 2);
insert into connection_instance values (10, 8, now(), 'Pociag7', 3);
insert into connection_instance values (11, 9, now() + interval '5 days', 'Pociag8', 3);
insert into connection_instance values (12, 10, now() + interval '6 days', 'Pociag9', 3);