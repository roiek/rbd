
insert INTO cities values  ('Wroclaw');
insert INTO cities values  ('Poznan');
insert INTO cities values  ('Gdansk');
insert INTO cities values  ('Warszawa');

insert into connection values (1);
insert into connection values (2);
insert into connection values (3);

insert into connection_cities values (1, 'Wroclaw', 0);
insert into connection_cities values (1, 'Poznan', 1);
insert into connection_cities values (1, 'Gdansk', 2);
insert into connection_cities values (2, 'Wroclaw', 0);
insert into connection_cities values (2, 'Gdansk', 1);
insert into connection_cities values (2, 'Poznan', 2);
insert into connection_cities values (3, 'Warszawa', 0);
insert into connection_cities values (3, 'Gdansk', 1);
insert into connection_cities values (3, 'Poznan', 2);


insert into connection_instance values (4, 10, sysdate, 'Pociag1', 1);
insert into connection_instance values (5, 11, sysdate+1, 'Pociag2', 1);
insert into connection_instance values (6, 12,sysdate+2, 'Pociag3', 1);
insert into connection_instance values (7, 8, sysdate, 'Pociag4', 2);
insert into connection_instance values (8, 9, sysdate+5, 'Pociag5', 2);
insert into connection_instance values (9, 10,sysdate+6, 'Pociag6', 2);
insert into connection_instance values (10, 8, sysdate, 'Pociag7', 3);
insert into connection_instance values (11, 9, sysdate+5, 'Pociag8', 3);
insert into connection_instance values (12, 10, sysdate+6, 'Pociag9', 3);